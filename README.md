# BlindTest Assets

Balancez toutes vos musiques de jeux vidéo

REGLES:

- les liens de video doivent se terminer par l'id de la vidéo. Pas d'arguments supplémentaires (voir liens dans exemple)

- la data "start" se compte en seconde

- pensez à bien respecter l'architecture d'un Asset


Architecture du fichier assets.json:
[
  {
    "name": "mario kart ds",
    "url": [
      {
        "link": "https://www.youtube.com/watch?v=x4OIAKCsOQk",
        "start": 3
      },
      {
        "link": "https://www.youtube.com/watch?v=w-b5s2smGkk",
        "start": 8
      }
    ]
  },
  {
    "name": "mario kart wii",
    "url": [
      {
        "link": "https://www.youtube.com/watch?v=JUPhS4nKEpw",
        "start": 0
      }
    ]
  }
]


Keybinds de l'application (menu help pas encore implémenté):
- Espace: Switch entre pause et play.
- F11: Switch entre plein écran et fenêtré.
- Escape: Si en plein écran, quitte le plein écran. Sinon retoune au menu.
- Flèche du haut: monte le volume.
- Flèche du bas: descend le volume.