const fs = require('fs');
const https = require('https');

const {google, Auth} = require('googleapis');
const tokens = require('./token.json');
const secret = require('./client_secret.json');
const games = require('../assets json/Game.json');
const animes = require('../assets json/Anime.json');
const films = require('../assets json/Film.json');
const musics = require('../assets json/Musique.json');
const series = require('../assets json/Serie.json');
const emissions = require('../assets json/Emission.json');

class PlaylistSorter {
    oauth2Client = new google.auth.OAuth2({
        clientId: secret.web.client_id,
        clientSecret: secret.web.client_secret
    });
    musicsToSort = [];
    totalSize = 0;

    constructor() {
        this.oauth2Client.setCredentials(tokens);
        console.log(this.oauth2Client._clientId);
        console.log('Starting My Amazing Playlist Checker...');

    }

    /**
     *
     * @param {object} music
     * @returns {MusicToSort}
     */
    searchVideo(query) {
        return new Promise((resolve, rejects) => {
            google.youtube("v3").search.list({
                part: 'snippet',
                q: query,
                auth: this.oauth2Client,
            })
            .then(res => {
                if (res.data.items.length > 0) {
                    resolve(res.data.items[0].id.videoId);
                } else {
                    resolve();
                }
            })
            .catch(err => rejects(err));
        });
        
    }

    checkIfExists(videoId) {
        return new Promise((resolve, reject) => {
            https.get(`https://i.ytimg.com/vi/${videoId}/mqdefault.jpg`, (res) => {
                if (res.statusCode == 200) {
                    resolve()
                } else {
                    reject()
                }
            }).on('error', (err) => console.log(err))
        })
    }

    fixUrl(assetName, url) {
        const videoId = url.link.split('v=')[1];
        return new Promise((resolve) => {
            this.checkIfExists(videoId).then(() => resolve(url)).catch(() => {
                console.log(`Assets ${assetName} - ${url.name} does not exists`)
                this.searchVideo(`${assetName} ${url.name}`).then(newVideoId => {
                    resolve({
                        name: url.name,
                        link: `${url.link.split('v=')[0]}v=${newVideoId}`,
                        start: url.start
                    })
                }).catch(err => console.error(err))
            })
        })
    }

    async run(assets, name) {
        // reset in case of multiple uses
        this.sortPlaylist = [];
        this.totalSize = assets.easy.length + assets.medium.length + assets.hard.length;

        const fixedAssets = Object.keys(assets).map(difficulty => Promise.all(assets[difficulty].map(asset => {
                return new Promise(resolve => {
                    Promise.all(asset.url.map(url => this.fixUrl(asset.title, url))).then(data => resolve({
                        title: asset.title,
                        url: data
                    }))
                })
            })
        ))

        const formattedAssets = {
            easy: await fixedAssets[0],
            medium: await fixedAssets[1],
            hard: await fixedAssets[2]
        }
        
        if (!fs.existsSync((`./${name}`))) {
            fs.mkdirSync(`./${name}`);
        }
        fs.writeFileSync(`./${name}/${name}.json`, JSON.stringify(formattedAssets));
    }
}


function main() {
    const playlistSorter = new PlaylistSorter();

    //playlistSorter.run(games, 'games');
    playlistSorter.run(animes, 'animes');
    playlistSorter.run(films, 'films');
    playlistSorter.run(musics, 'musics');
    playlistSorter.run(series, 'series');
    //playlistSorter.run(emissions, 'emissions');
    console.log("Finished !!");
}

main();