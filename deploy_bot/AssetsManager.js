const firebase = require("./FirebaseConfig");

module.exports = class AssetsManager {
    /** @param {$ObjMap} assets
     * @param {string} docName
     * **/
    static setAssetsOfCollection(assets, docName) {
        const description = {
            description: docName
        };
        
        //firebase.firestore().collection('assets').doc(docName).collection('easy').set(description)
        assets.easy.forEach(asset => {
            asset.url.forEach(url => {
                firebase.firestore().collection(docName).doc(docName).collection('easy').doc(asset.title).add(url)
            })
        })
        //firebase.firestore().collection('assets').doc(docName).collection('medium').set(description)
        assets.medium.forEach(asset => {
            asset.url.forEach(url => {
                firebase.firestore().collection(docName).doc(docName).collection('medium').doc(asset.title).add(url)
            })
        })
        //firebase.firestore().collection('assets').doc(docName).collection('hard').set(description)
        assets.hard.forEach(asset => {
            asset.url.forEach(url => {
                firebase.firestore().collection(docName).doc(docName).collection('hard').doc(asset.title).add(url)
            })
        })
    }
}

