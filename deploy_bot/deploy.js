// Init Firestore
const AssetsManager =  require('./AssetsManager');

// Loading assets
const games = require('../assets json/Game.json');
const animes = require('../assets json/Anime.json');
const films = require( '../assets json/Film.json');
const musics = require('../assets json/Musique.json');

// Update Firestore for each assets
AssetsManager.setAssetsOfCollection(games, 'games');
AssetsManager.setAssetsOfCollection(animes, 'animes');
AssetsManager.setAssetsOfCollection(films, 'films');
AssetsManager.setAssetsOfCollection(musics, 'musics');



